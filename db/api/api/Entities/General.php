<?php
/**
 * Created by PhpStorm.
 * User: rikimaru
 * Date: 20.03.15
 * Time: 20:37
 */

class GeneralEntity {

    public function methodClear()
    {

        if (count($_POST) == 0) {
            $connection = Yii::app()->db;

            $sql = "SET FOREIGN_KEY_CHECKS=0;
                   TRUNCATE TABLE forum;
                   TRUNCATE TABLE post;
                   TRUNCATE TABLE user;
                   TRUNCATE TABLE followers;
                   TRUNCATE TABLE subscriptions;
                   TRUNCATE TABLE thread;
                   SET FOREIGN_KEY_CHECKS=1;";

            $command = $connection->createCommand($sql);
            $command->execute();

            $code = 0;
            $status = "OK";
        } else {
            $code = 2;
            $status = "Invalid JSON";
        }

        $status = array('code' => $code, 'response' => $status);
        echo json_encode($status);
    }

    public function methodTest() {
        echo "OK";
    }

    public function methodStatus() {
        if (count($_POST) == 0) {
            $connection = Yii::app()->db;
            $response = [];
            $response["response"] = [];
            foreach (["user","thread","forum","post"] as $key => $value){
                $sql = "SELECT COUNT(*) AS cnt FROM $value";
                $command = $connection->createCommand($sql);
                $command->execute();
                $res = $command->queryAll();
                $response["response"][$value] = $res[0]["cnt"];
            }
            $code = 0;
        } else {
            $code = 2;
        }
        $status = array('code' => $code, 'response' => $response["response"]);
        echo json_encode($status);
    }

}