<?php
/**
 * Created by PhpStorm.
 * User: rikimaru
 * Date: 20.03.15
 * Time: 20:38
 */

class Request {

    public static $entity;
    public static $method;

    public function __construct() {
        $request = explode("?", $_SERVER['REQUEST_URI']);
        $request = explode("/", $request[0]);
        Request::$entity = $request[3];
        if( !isset($request[4]) || empty($request[4]) ) {
            Request::$method = null;
        } else {
            Request::$method = $request[4];
        }
    }
}

class Router {

    public static function start() {
        new Request();

        $method = "method";
        $entityClass = "Entity";

        if( is_null(Request::$method) ) {
            $entityFile = "General";
            $entityClass = "General".$entityClass;
            $method .= ucfirst(Request::$entity);
        } else {

            $entityFile = ucfirst(Request::$entity);
            $entityClass = $entityFile . $entityClass;
            $method .= ucfirst(Request::$method);
        }

        include_once "api/Helper.php";
        include_once "api/Entities/".$entityFile.".php";
        $entityObj = new $entityClass();
        $entityObj->$method();
    }
}