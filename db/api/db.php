<?php
/**
 * Created by PhpStorm.
 * User: rikimaru
 * Date: 20.03.15
 * Time: 20:58
 */

class Yii {
    private function __construct() {}

    private static $instanse;
    public $db;

    public static function app() {
        if( self::$instanse == null ) {
            self::$instanse = new self;
            self::$instanse->db = new DB();
        }
        return self::$instanse;
    }

}

class DB {
    private $F;

    public function __construct() {
        $config = Config::$db;

        $this->F = new PDO("mysql:dbname=".$config['db']['db_name'].';host='.$config['db']['host'].";charset=utf8", $config['db']['user'], $config['db']['pass']);
        $this->F->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function createCommand($command) {
        $q = new Command($this->F->prepare($command));
        return $q;
    }

    public function getLastInsertID() {
        return $this->F->lastInsertId();

    }
}

class Command {

    private $F;
    public function __construct($command){
        $this->F = $command;
    }

    public function bindParam($param, $paramVal) {
        $this->F->bindParam($param, $paramVal);
    }

    public function queryAll() {
        $this->F->execute();
        return $this->F->fetchAll(PDO::FETCH_ASSOC);
    }

    public function execute() {
        $this->F->execute();
    }
}